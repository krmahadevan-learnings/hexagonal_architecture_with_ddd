# Hexagonal Architecture Demo Project

This project is a sample to show how to create/work with hexagonal architectures using Springboot.

It also leverages domain driven design as an approach.

The sample code is a complete rip off from https://github.com/eugenp/tutorials/tree/master/ddd

The only reason why this sample code exists in this repository is because its easy to find it when this is a separate repository.