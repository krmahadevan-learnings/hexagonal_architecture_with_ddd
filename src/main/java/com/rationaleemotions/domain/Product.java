package com.rationaleemotions.domain;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.math.BigDecimal;
import java.util.UUID;

public record Product(UUID id, BigDecimal price, String name) {
    @JsonCreator
    public Product(
            @JsonProperty("id") final UUID id,
            @JsonProperty("price") final BigDecimal price,
            @JsonProperty("name") final String name
    ) {
        this.id = id;
        this.price = price;
        this.name = name;
    }

    public static Product from(OrderItem orderItem) {
        return new Product(orderItem.getProductId(), orderItem.getPrice(), "");
    }
}
