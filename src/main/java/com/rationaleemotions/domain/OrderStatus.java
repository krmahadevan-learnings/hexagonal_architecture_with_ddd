package com.rationaleemotions.domain;

public enum OrderStatus {
    CREATED, COMPLETED
}
