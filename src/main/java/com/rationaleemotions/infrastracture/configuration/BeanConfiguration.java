package com.rationaleemotions.infrastracture.configuration;

import com.rationaleemotions.HexagonalArchitectureWithDddApplication;
import com.rationaleemotions.domain.repository.OrderRepository;
import com.rationaleemotions.domain.service.DomainOrderService;
import com.rationaleemotions.domain.service.OrderService;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration(proxyBeanMethods = false)
@ComponentScan(basePackageClasses = HexagonalArchitectureWithDddApplication.class)
public class BeanConfiguration {

    @Bean
    OrderService orderService(final OrderRepository orderRepository) {
        return new DomainOrderService(orderRepository);
    }
}
