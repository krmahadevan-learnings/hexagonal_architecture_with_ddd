package com.rationaleemotions.application.response;

import java.util.UUID;

public record CreateOrderResponse(UUID id) {
}
