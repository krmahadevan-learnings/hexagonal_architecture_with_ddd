package com.rationaleemotions.application.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.rationaleemotions.domain.Product;
import lombok.Getter;

@Getter
public class AddProductRequest {
    private final Product product;

    @JsonCreator
    public AddProductRequest(@JsonProperty("product") final Product product) {
        this.product = product;
    }
}
