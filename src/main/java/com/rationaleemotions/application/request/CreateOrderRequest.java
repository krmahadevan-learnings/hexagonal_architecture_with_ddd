package com.rationaleemotions.application.request;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.rationaleemotions.domain.Product;
import lombok.Getter;

@Getter
public class CreateOrderRequest {
    private final Product product;

    @JsonCreator
    public CreateOrderRequest(@JsonProperty("product") final Product product) {
        this.product = product;
    }

}
