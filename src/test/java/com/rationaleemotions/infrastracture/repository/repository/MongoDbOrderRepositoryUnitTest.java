package com.rationaleemotions.infrastracture.repository.repository;

import com.rationaleemotions.domain.Order;
import com.rationaleemotions.domain.Product;
import com.rationaleemotions.infrastracture.repository.mongo.MongoDbOrderRepository;
import com.rationaleemotions.infrastracture.repository.mongo.SpringDataMongoOrderRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.math.BigDecimal;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;
import static org.mockito.Mockito.*;

class MongoDbOrderRepositoryUnitTest {
    private SpringDataMongoOrderRepository springDataOrderRepository;
    private MongoDbOrderRepository tested;

    @BeforeEach
    void setUp() {
        springDataOrderRepository = mock(SpringDataMongoOrderRepository.class);
        tested = new MongoDbOrderRepository(springDataOrderRepository);
    }

    @Test
    void shouldFindById_thenReturnOrder() {
        UUID id = UUID.randomUUID();
        Order order = createOrder(id);
        when(springDataOrderRepository.findById(id)).thenReturn(Optional.of(order));

        Optional<Order> result = tested.findById(id);
        assertTrue(result.isPresent());
        assertEquals(order, result.get());
    }

    @Test
    void shouldSaveOrder_viaSpringDataOrderRepository() {
        final UUID id = UUID.randomUUID();
        final Order order = createOrder(id);
        tested.save(order);
        verify(springDataOrderRepository).save(order);
    }

    private Order createOrder(UUID id) {
        return new Order(id, new Product(UUID.randomUUID(), BigDecimal.TEN, "product"));
    }
}