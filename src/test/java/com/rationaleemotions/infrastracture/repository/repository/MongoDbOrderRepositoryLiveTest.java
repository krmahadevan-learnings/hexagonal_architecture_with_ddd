package com.rationaleemotions.infrastracture.repository.repository;

import com.rationaleemotions.domain.Order;
import com.rationaleemotions.domain.Product;
import com.rationaleemotions.domain.repository.OrderRepository;
import com.rationaleemotions.infrastracture.repository.mongo.SpringDataMongoOrderRepository;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.data.mongo.DataMongoTest;
import org.springframework.boot.test.context.TestConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.test.context.DynamicPropertyRegistry;
import org.springframework.test.context.DynamicPropertySource;
import org.testcontainers.containers.GenericContainer;
import org.testcontainers.containers.wait.strategy.Wait;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;
import org.testcontainers.utility.DockerImageName;
import org.testcontainers.utility.MountableFile;

import java.math.BigDecimal;
import java.time.Duration;
import java.util.Map;
import java.util.Optional;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

@Testcontainers
@DataMongoTest
class MongoDbOrderRepositoryLiveTest {

    public static final int MONGO_DB_PORT = 27017;
    @Container
    public static GenericContainer<?> mongoDBContainer =
            new GenericContainer<>(DockerImageName.parse("mongo:5.0"))
                    .withExposedPorts(MONGO_DB_PORT)
                    .withEnv(
                            Map.of("MONGO_INITDB_ROOT_USERNAME", "admin",
                                    "MONGO_INITDB_ROOT_PASSWORD", "admin",
                                    "MONGO_INITDB_DATABASE", "order-database")
                    )
                    .withCopyToContainer(
                            MountableFile.forHostPath("src/test/resources/mongo-init.js"),
                            "/docker-entrypoint-initdb.d/mongo-init.js")
                    .waitingFor(Wait.forLogMessage("(?i).*waiting for connections.*", 2))
                    .withReuse(true)
                    .withStartupTimeout(Duration.ofSeconds(50));
    @Autowired
    private SpringDataMongoOrderRepository mongoOrderRepository;

    @Autowired
    private OrderRepository orderRepository;

    @AfterEach
    void cleanUp() {
        mongoOrderRepository.deleteAll();
    }

    @DynamicPropertySource
    public static void overrideProperties(DynamicPropertyRegistry registry) {
        registry.add("spring.data.mongodb.port", () -> mongoDBContainer.getMappedPort(MONGO_DB_PORT));
    }

    @Test
    void shouldFindById_thenReturnOrder() {
        final UUID id = UUID.randomUUID();
        final Order order = createOrder(id);

        orderRepository.save(order);

        final Optional<Order> result = orderRepository.findById(id);

        assertTrue(result.isPresent());

        assertEquals(order, result.get());
    }

    private Order createOrder(UUID id) {
        return new Order(id, new Product(UUID.randomUUID(), BigDecimal.TEN, "product"));
    }

    //This is required because the test class resides in a sub directory and the JPA classes reside in a parent
    //directory under `src/main/java`
    @TestConfiguration
    @ComponentScan(basePackages = "com.rationaleemotions")
    public static class MyConfiguration { }
}